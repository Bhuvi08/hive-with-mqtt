import 'dart:convert';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_switch/flutter_switch.dart';
import 'package:hive/hive.dart';
import 'package:mqtt_client/mqtt_client.dart';
import 'package:lottie/lottie.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:universal_mqtt_client/universal_mqtt_client.dart';
import '../data_model.dart';
import 'package:hive_flutter/hive_flutter.dart';

// ignore: camel_case_types
class dashboardwithouttab extends StatefulWidget with ChangeNotifier {
  @override
  _dashboardwithouttab createState() => _dashboardwithouttab();
}

class _dashboardwithouttab extends State<dashboardwithouttab> {
  bool status = false;
  bool on_off = true;
  var _loadingInProgress_status = false;
  var loading = false;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  List image = [ 'assets/phlevel.png'];
  int motordatacount, levelcount, flowmetercount, qacount;
  var leveldata,
      motordatas,
      motordata,
      flowmeterdata,
      qadata,
      mqtttopic,
      mqtthost,
      mqttport,
      parsedJson,
      ownerlogo,
      firsttankid,
      firsttankname,
      firstweighbridge,
      firstqualityid,
      firstqualityname,
      runhistory,
      motordatavalue,
      motorstatusvalue,
      powervalue,
      starttime,
      idvalue,
      flowdata,
      qualitydata,
      flowdetailsid;
  int index = 0;
  var firstmotorid, firstmotorname, firstmotorpower, firstmotorstatus;
  List<LevelDetail> leveldatavaluelist = [];
  List<FlowDetail> flowdatavaluelist = [];
  List<MotorDetail> motordatavaluelist = [];
  List<Runhistroy> motorrunhistory = [];
  List<QualityData> qualitydatas = [];
  List jsonList = [];
  late ChangeNotifier notifierss;
  var mqttdata;
  var mqttdatas;
  var jsondata;
  FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
  FlutterLocalNotificationsPlugin();
  var myDynamicAspectRatio = 1000 / 1;

  _setMessage(Map<String, dynamic> message) {
    final notification = message['notification'];
    final data = message['data'];
    final String title = notification['title'];
    final String body = notification['body'];
    String mMessage = data['page'];
  }

  showNotification(Map<String, dynamic> message) async {
    final notification = message['notification'];
    final String title = notification['title'];
    final String body = notification['body'];
    var android = new AndroidNotificationDetails(
        'channel id', 'channel NAME', 'CHANNEL DESCRIPTION',
        priority: Priority.high, importance: Importance.max);
    var iOS = new IOSNotificationDetails();
    var platform = new NotificationDetails(android: android, iOS: iOS);
    await flutterLocalNotificationsPlugin.show(0, title, body, platform,
        payload: 'Push Test Success');
  }

  void getMessage() {
    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print('on message: $message');
        showNotification(message);
        return _setMessage(message);
      },
      onResume: (Map<String, dynamic> message) async {
        print('on resume: $message');
        return _setMessage(message);
      },
      onLaunch: (Map<String, dynamic> message) async {
        print('on launch: $message');
        return _setMessage(message);
      },
    );
    _firebaseMessaging.requestNotificationPermissions(
      const IosNotificationSettings(sound: true, badge: true, alert: true),
    );
  }

  Future<Null> getData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var box = await Hive.openBox<DataModel>('datamodel');
    //Return String
    setState(() {
      status = true;
    });
    final responseData = await http
        .post(baseurl + versions + appinfo, body: {"login": loginuserid});
    if (responseData.statusCode == 200) {
      final data = responseData.body;
      print(data);
      print("data");
      final statusvalue = jsonDecode(data)['status'];
      print(statusvalue);
      if (statusvalue == '1') {
        motordatacount = jsonDecode(data)['response']['motor'];
        levelcount = jsonDecode(data)['response']['level'];
        flowmetercount = jsonDecode(data)['response']['flow_meter'];
        qacount = jsonDecode(data)['response']['qa'];
        motordata = jsonDecode(data)['response']['motor_details'] as List;
        leveldata = jsonDecode(data)['response']['level_details'] as List;
        flowdata = jsonDecode(data)['response']['flow_details'] as List;
        qualitydata = jsonDecode(data)['response']['qa_details'] as List;
        mqtttopic = jsonDecode(data)['response']['mqtt']['topic'];
        mqtthost = jsonDecode(data)['response']['mqtt']['host'];
        mqttport = jsonDecode(data)['response']['mqtt']['port'];
        ownerlogo = jsonDecode(data)['response']['owner']['logo'];
        // _connect();
        setState(() {
          for (Map i in leveldata) {
            leveldatavaluelist.add(LevelDetail.fromMap(i));
          }
          for (Map i in motordata) {
            motordatavaluelist.add(MotorDetail.fromMap(i));
          }
          for (Map i in flowdata) {
            flowdatavaluelist.add(FlowDetail.fromMap(i));
          }
          for (Map i in qualitydata) {
            qualitydatas.add(QualityData.fromMap(i));
          }
          firsttankid = leveldatavaluelist.isEmpty
              ? firsttankid = ''
              : firsttankid = leveldatavaluelist[0].id;
          firsttankname = leveldatavaluelist.isEmpty
              ? firsttankname = ''
              : firsttankname = leveldatavaluelist[0].tankName;
          firstweighbridge = leveldatavaluelist.isEmpty
              ? firstweighbridge = ''
              : firstweighbridge = leveldatavaluelist[0].weighbridge;
          firstmotorid = motordatavaluelist.isEmpty
              ? firstmotorid = ''
              : firstmotorid = motordatavaluelist[0].id;
          firstqualityid = qualitydatas.isEmpty
              ? firstqualityid = ''
              : firstqualityid = qualitydatas[0].id;
          firstqualityname = qualitydatas.isEmpty
              ? firstqualityname = ''
              : firstqualityname = qualitydatas[0].name;
          saveValue(
              firsttankid,
              firsttankname,
              firstweighbridge,
              mqtttopic,
              mqtthost,
              mqttport,
              motordatacount,
              levelcount,
              flowmetercount,
              flowdetailsid,
              qacount,
              ownerlogo,
              firstmotorid,
              firstqualityid,
              firstmotorname,
              firstmotorpower,
              firstmotorstatus);
          status = false;
        });
        if (mounted) {
          setState(() {
            for (var i = 0; i <= leveldatavaluelist.length - 1; i++) {
              List lists = [];
              lists.add(leveldatavaluelist[i].currentpercentage);
              box.put(
                  'datamodel',
                  DataModel(
                      leveldata: leveldatavaluelist[i].currentpercentage));
            }
            box.listenable();
            mqttdata = box.get('datamodel');
          });
        }
      }
    } else {
      status = false;
    }
    _connect();
  }

  saveValue(
      firsttankid,
      firsttankname,
      firstweighbridge,
      mqtttopic,
      mqtthost,
      mqttport,
      motordatacount,
      levelcount,
      flowmetercount,
      flowdetailsid,
      qacount,
      ownerlogo,
      firstmotorid,
      firstqualityid,
      firstmotorname,
      firstmotorpower,
      firstmotorstatus) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('firsttankid', firsttankid);
    prefs.setString('firsttankname', firsttankname);
    prefs.setString('firstweighbridge', firstweighbridge);
    prefs.setString('mqtttopic', mqtttopic);
    prefs.setString('mqtthost', mqtthost);
    prefs.setString('mqttport', mqttport);
    prefs.setInt('motordatacount', motordatacount);
    prefs.setInt('levelcount', levelcount);
    prefs.setInt('flowmetercount', flowmetercount);
    prefs.setInt('qacount', qacount);
    prefs.setString('ownerlogo', ownerlogo);
    prefs.setString('firstmotorid', firstmotorid);
    prefs.setString('firstqualityid', firstqualityid);
    prefs.setString('firstqualityname', firstqualityname);
    prefs.setString('firstmotorname', firstmotorname);
    prefs.setString('firstmotorpower', firstmotorpower);
    prefs.setString('firstmotorstatus', firstmotorstatus);
  }

  getstatus(idvalue) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String loginuserid = prefs.getString('loginuserid');
    setState(() {
      _loadingInProgress_status = true;
    });
    final responseData = await http.post(baseurl + versions + motordetails,
        body: {"cid": loginuserid, "motor_id": idvalue});
    print(responseData.statusCode);
    if (responseData.statusCode == 200) {
      final data = responseData.body;
      final String statusvalue = jsonDecode(data)['status'].toString();
      print(data);
      if (statusvalue == '1') {
        motorstatusvalue = jsonDecode(data)['details']['motor_status'];
        if (mounted)
          setState(() {
            _loadingInProgress_status = false;
            Navigator.of(context).pop();
          });
      }
    } else {
      _loadingInProgress_status = false;
    }
  }

  getstatuschange(idvalue) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String loginuserid = prefs.getString('loginuserid');
    setState(() {
      _loadingInProgress_status = true;
    });
    final responseData =
    await http.post(baseurl + versions + motordetailstatus, body: {
      "cid": loginuserid,
      "motor_id": idvalue,
      "status_change": motorstatusvalue == "On" ? "Off" : "On"
    });
    print(responseData.statusCode);
    if (responseData.statusCode == 200) {
      final data = responseData.body;
      final String statusvalue = jsonDecode(data)['status'].toString();
      print(data);
      if (statusvalue == '1') {
        setState(() {
          _loadingInProgress_status = false;
          getstatus(idvalue);
        });
      }
    } else {
      _loadingInProgress_status = false;
    }
  }


  _connect() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var box = await Hive.openBox<DataModel>('datamodel');
    String mqtttopic = prefs.getString('mqtttopic');
    String mqtthost = prefs.getString('mqtthost');
    String mqttport = prefs.getString('mqttport');
    String mqqthostlink = "tcp://$mqtthost:$mqttport";
    print(mqtttopic);
    print(mqtthost);
    print(mqttport);
    print(mqqthostlink);
    print("mqqthostlink");
    final client = UniversalMqttClient(broker: Uri.parse(mqqthostlink));
    await client.connect();
    final subscription = client.handleString(mqtttopic, MqttQos.atMostOnce);
    subscription.listen((event) {
      print("Device status: $event");
    });
    client.publishString(
        mqtttopic, 'Connected and running!', MqttQos.atLeastOnce);
    print("subscription");
    client.status.listen((status) async {
      print("Status $status");
      subscription.listen((message) {
        print('messages: $message');
        if (mounted) {
          setState(() {
            parsedJson = json.decode(message);
            box.put(
                'datamodel',
                DataModel(
                  mqttId: parsedJson[0],
                  mqttValue: parsedJson[1],
                ));
            mqttdatas = box.get('datamodel');
            for (var i = 0; i <= leveldatavaluelist.length - 1; i++) {
              print("leveldatavaluelist[i]");
              print(leveldatavaluelist.length);
              print(leveldatavaluelist[i].currentpercentage);
              if (leveldatavaluelist[i].id.contains(parsedJson[0])) {
                leveldatavaluelist[i].currentpercentage = mqttdatas.mqttValue;
              }
            }
            for (var i = 0; i <= flowdatavaluelist.length - 1; i++) {
              print("flowdatavaluelist[i]");
              print(flowdatavaluelist.length);
              print(flowdatavaluelist[i].currrentFlow);
              if (flowdatavaluelist[i].id.contains(parsedJson[0])) {
                flowdatavaluelist[i].currrentFlow = mqttdatas.mqttValue;
              }
            }
            for (var i = 0; i <= qualitydatas.length - 1; i++) {
              print("qualitydatas[i]");
              print(qualitydatas.length);
              print(qualitydatas[i].currentvalue);
              if (qualitydatas[i].id.contains(parsedJson[0])) {
                qualitydatas[i].currentvalue = mqttdatas.mqttValue;
              }
            }
            box.listenable();
          });
        }
      });
    });
  }

  disconnect() async {
    String mqqthostlink = "tcp://" + mqtthost + ":" + mqttport;
    final client = UniversalMqttClient(broker: Uri.parse(mqqthostlink));
    await client.connect();
    final subscription = client.handleString(mqtttopic, MqttQos.atMostOnce);
    client.disconnect();
    client.status.listen((status) {
      print('Connection Status: $status');
    });
  }

  void initState() {
    getData();
    // connect();
    _firebaseMessaging.getToken().then((token) => print("FCM: $token"));
    getMessage();
    flutterLocalNotificationsPlugin = new FlutterLocalNotificationsPlugin();
    var android = new AndroidInitializationSettings('@mipmap/ic_launcher');
    var iOS = new IOSInitializationSettings();
    var initSetttings = new InitializationSettings(android: android, iOS: iOS);
    flutterLocalNotificationsPlugin.initialize(initSetttings,
        onSelectNotification: onSelectNotification);
    super.initState();
  }

  Future onSelectNotification(String payload) {
    debugPrint("payload : $payload");
  }

  @override
  void dispose() {
    super.dispose();
    disconnect();
  }

  @override
  Widget build(BuildContext context) {
    Size _size = MediaQuery.of(context).size;
    print(_size.width);
    print("_size.width");
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        key: _scaffoldKey,
        drawer: drawers,
        backgroundColor: Colors.white,
        appBar: AppBar(
          toolbarHeight: _size.width>=600  ? 100 : 60,
          backgroundColor: Colors.white,
          elevation: 0,
          leading: GestureDetector(
            onTap: () {
              _scaffoldKey.currentState.openDrawer();
            },
            child: Icon(
              Icons.menu,
              color: Colors.black,
              size: _size.width>=600  ? 40 : 30,
            ),
          ),
          title: Text(
            'Dashboard',
            style: TextStyle(
                color: Color(0xff4B4B4B),
                fontFamily: 'Proxima',
                fontSize: _size.width>=600  ? 30 : 20,
                fontWeight: FontWeight.bold),
          ),
        ),
        body: OrientationBuilder(
          builder: (context, orientation){
            if(orientation == Orientation.portrait){
              return NotificationListener<OverscrollIndicatorNotification>(
                child: SingleChildScrollView(
                    child: status
                        ? Container(
                      alignment: Alignment.center,
                      height: MediaQuery.of(context).size.height / 1.3,
                      width: MediaQuery.of(context).size.width,
                      child: Lottie.asset('assets/loading.json',
                          height: _size.width>=600  ? 80 : 60, width: _size.width>=600  ? 80 : 60),
                    )
                        : Column(
                      children: [
                        levelcount != 0
                            ? Container(
                          padding: EdgeInsets.only(
                              left:
                              MediaQuery.of(context).size.width / 30),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Text(
                                'Quantity Report',
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: _size.width>=600  ? MediaQuery.of(context).size.height/50 : 15,
                                    fontWeight: FontWeight.bold,
                                    fontFamily: 'Proxima'),
                              ),
                            ],
                          ),
                        )
                            : Container(),
                        levelcount != 0
                            ? levelcount != null
                            ? GridView.count(
                            shrinkWrap: true,
                            physics: NeverScrollableScrollPhysics(),
                            crossAxisCount: 2,
                            crossAxisSpacing: _size.width>=600  ? 60 : 10,
                            mainAxisSpacing: _size.width>=600  ? 20 : 10,
                            childAspectRatio:
                            (_size.width>=600  ? MediaQuery.of(context).size.width /
                                (MediaQuery.of(context).size.height / 1.7) : 4 / 3),
                            children: List.generate(
                              leveldatavaluelist.length,
                                  (index) => GestureDetector(
                                  onTap: () {
                                  },
                                  child: Container(
                                    margin: _size.width >=600? EdgeInsets.all(45):EdgeInsets.all(20),
                                    decoration: BoxDecoration(
                                        borderRadius:
                                        BorderRadius.all(
                                            Radius.circular(5)),
                                        shape: BoxShape.rectangle,
                                        color: Colors.white,
                                        boxShadow: [
                                          BoxShadow(
                                            color: Colors.grey
                                                .withOpacity(0.1),
                                            spreadRadius: 3,
                                            blurRadius: 3,
                                            offset: Offset(0,
                                                3), // changes position of shadow
                                          ),
                                        ]),
                                    child: Column(
                                        crossAxisAlignment:
                                        CrossAxisAlignment
                                            .center,
                                        children: [
                                          SizedBox(
                                            height: _size.width>=600  ? MediaQuery.of(context).size.height/40 : 8,
                                          ),
                                          Container(
                                              height:  _size.width>=600? MediaQuery.of(context).size.height/20:30,
                                              padding:
                                              EdgeInsets.only(
                                                  top: _size.width>=600
                                                      ? MediaQuery.of(context).size.height/90
                                                      : 3,
                                                  right: 10,
                                                  left: 10),
                                              child: Text(
                                                leveldatavaluelist[
                                                index]
                                                    .tankName,
                                                style: TextStyle(
                                                    color: Colors
                                                        .black,
                                                    fontSize: _size.width>=600
                                                        ? MediaQuery.of(context).size.height/50
                                                        : 13,
                                                    fontFamily:
                                                    'Proxima',
                                                    fontWeight:
                                                    FontWeight
                                                        .bold),
                                                textAlign: TextAlign
                                                    .center,
                                              )),
                                          Container(
                                            margin: EdgeInsets.only(
                                                top: _size.width>=600
                                                    ? MediaQuery.of(context).size.height/50
                                                    : 5),
                                            child: Row(
                                              mainAxisAlignment:
                                              MainAxisAlignment
                                                  .spaceAround,
                                              children: [
                                                Row(
                                                  children: [
                                                    Container(
                                                      padding: EdgeInsets.only(
                                                          right: MediaQuery.of(context)
                                                              .size
                                                              .width /
                                                              24),
                                                      child: Image
                                                          .asset(
                                                        'assets/tanks.png',
                                                        scale: _size.width>=600
                                                            ? 0.6
                                                            : 1,
                                                      ),
                                                    ),
                                                    Column(
                                                      crossAxisAlignment:
                                                      CrossAxisAlignment
                                                          .start,
                                                      children: [
                                                        Container(
                                                            child:
                                                            Text(
                                                              "${leveldatavaluelist[index].label} ${leveldatavaluelist[index].unit}",
                                                              style: TextStyle(
                                                                  color: Colors
                                                                      .black54,
                                                                  fontSize: _size.width>=600
                                                                      ? MediaQuery.of(context).size.height/50
                                                                      : 13,
                                                                  fontFamily:
                                                                  'Proxima'),
                                                            )),
                                                        SizedBox(
                                                          height: 8,
                                                        ),
                                                        Container(
                                                            child: Text(
                                                                parsedJson != null
                                                                    ? leveldatavaluelist[index].id.contains(parsedJson[0])
                                                                    ? mqttdatas.mqttValue
                                                                    : leveldatavaluelist[index].currentpercentage
                                                                    : leveldatavaluelist[index].currentpercentage,
                                                                style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: _size.width>=600  ? MediaQuery.of(context).size.height/50 : 13, fontFamily: 'Proxima'))),
                                                      ],
                                                    )
                                                  ],
                                                ),
                                              ],
                                            ),
                                          ),
                                        ]),
                                  )),
                            ))
                            : Container()
                            : Container(),
                        qacount != 0
                            ? Container(
                          padding: EdgeInsets.only(
                              left:
                              MediaQuery.of(context).size.width / 30),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Text(
                                'Quality Report',
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: _size.width>=600  ? MediaQuery.of(context).size.height/50 : 15,
                                    fontWeight: FontWeight.bold,
                                    fontFamily: 'Proxima'),
                              ),
                            ],
                          ),
                        )
                            : Container(),
                        qacount != 0
                            ? GridView.builder(
                            shrinkWrap: true,
                            physics: NeverScrollableScrollPhysics(),
                            gridDelegate:
                            SliverGridDelegateWithFixedCrossAxisCount(
                                crossAxisCount: 2,
                                childAspectRatio:
                                _size.width>=600  ?MediaQuery.of(context).size.width /
                                    (MediaQuery.of(context).size.height / 1.8) : 4 / 3,
                                crossAxisSpacing: _size.width>=600  ? 60 : 10,
                                mainAxisSpacing: _size.width>=600  ? 20 : 10),
                            itemCount: qualitydatas.length,
                            itemBuilder: (ctx, index) {
                              return GestureDetector(
                                onTap: () {
                                },
                                child: Container(
                                  margin: _size.width >=600? EdgeInsets.all(45):EdgeInsets.all(20),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(5)),
                                      shape: BoxShape.rectangle,
                                      color: Colors.white,
                                      boxShadow: [
                                        BoxShadow(
                                          color: Colors.grey
                                              .withOpacity(0.1),
                                          spreadRadius: 3,
                                          blurRadius: 3,
                                          offset: Offset(0,
                                              3), // changes position of shadow
                                        ),
                                      ]),
                                  child: Column(
                                    children: [
                                      Container(
                                          padding:
                                          EdgeInsets.only(top: 10),
                                          child: Text(
                                            qualitydatas[index].name,
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontSize:
                                                _size.width>=600  ? MediaQuery.of(context).size.height/50 : 15,
                                                fontFamily: 'Proxima',
                                                fontWeight:
                                                FontWeight.bold),
                                          )),
                                      SizedBox(
                                        height: _size.width>=600  ? 20 : 20,
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                        MainAxisAlignment
                                            .spaceAround,
                                        children: [
                                          Row(
                                            children: [
                                              Container(
                                                padding: EdgeInsets.only(
                                                    right: MediaQuery.of(
                                                        context)
                                                        .size
                                                        .width /
                                                        24),
                                                child: Image.asset(
                                                  'assets/phlevel.png',
                                                  scale: _size.width>=600
                                                      ? 2.5
                                                      : 3.5,
                                                ),
                                              ),
                                              Column(
                                                crossAxisAlignment:
                                                CrossAxisAlignment
                                                    .start,
                                                children: [
                                                  Container(
                                                      child: Text(
                                                        'Current',
                                                        style: TextStyle(
                                                            color: Colors
                                                                .black54,
                                                            fontSize: _size.width>=600
                                                                ? MediaQuery.of(context).size.height/50
                                                                : 13,
                                                            fontFamily:
                                                            'Proxima'),
                                                      )),
                                                  SizedBox(
                                                    height: 5,
                                                  ),
                                                  Container(
                                                      child: Text(
                                                          parsedJson !=
                                                              null
                                                              ? qualitydatas[index].id.contains(parsedJson[
                                                          0])
                                                              ? mqttdatas
                                                              .mqttValue
                                                              : qualitydatas[index]
                                                              .currentvalue
                                                              : qualitydatas[
                                                          index]
                                                              .currentvalue,
                                                          style: TextStyle(
                                                              color: Color(
                                                                  0xff002251),
                                                              fontWeight:
                                                              FontWeight
                                                                  .bold,
                                                              fontSize:
                                                              _size.width>=600
                                                                  ? MediaQuery.of(context).size.height/50
                                                                  : 15,
                                                              fontFamily:
                                                              'Proxima')))
                                                ],
                                              )
                                            ],
                                          )
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              );
                            })
                            : Container(),
                        motordatacount != 0
                            ? Container(
                          padding: EdgeInsets.only(
                              left:
                              MediaQuery.of(context).size.width / 30),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Text(
                                'Motor Status',
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: _size.width>=600  ? MediaQuery.of(context).size.height/50 : 15,
                                    fontWeight: FontWeight.bold,
                                    fontFamily: 'Proxima'),
                              ),
                            ],
                          ),
                        )
                            : Container(),
                        motordatacount != 0
                            ? GridView.count(
                            shrinkWrap: true,
                            childAspectRatio:
                            (_size.width>=600 ? MediaQuery.of(context).size.width /
                                (MediaQuery.of(context).size.height / 1.65) : 4.1 / 4),
                            crossAxisSpacing: _size.width>=600  ? 60 : 10,
                            physics: NeverScrollableScrollPhysics(),
                            crossAxisCount: 2,
                            children: List.generate(
                              motordatacount,
                                  (index) => GestureDetector(
                                onTap: () {
                                },
                                child: Container(
                                  margin: _size.width >=600? EdgeInsets.all(40):EdgeInsets.all(20),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(5)),
                                      shape: BoxShape.rectangle,
                                      color: Colors.white,
                                      boxShadow: [
                                        BoxShadow(
                                          color: Colors.grey
                                              .withOpacity(0.1),
                                          spreadRadius: 3,
                                          blurRadius: 3,
                                          offset: Offset(0,
                                              3), // changes position of shadow
                                        ),
                                      ]),
                                  child: Column(children: [
                                    Container(
                                        padding:
                                        EdgeInsets.only(top: MediaQuery.of(context).size.height/70),
                                        child: Text(
                                          motordatavaluelist[index]
                                              .motorName,
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontSize:
                                              _size.width>=600  ? MediaQuery.of(context).size.height/50 : 15,
                                              fontFamily: 'Proxima',
                                              fontWeight:
                                              FontWeight.bold),
                                        )),
                                    Container(
                                      margin: EdgeInsets.only(
                                          top: _size.width>=600  ? MediaQuery.of(context).size.height/40 : 10),
                                      child: Row(
                                        mainAxisAlignment:
                                        MainAxisAlignment
                                            .spaceAround,
                                        children: [
                                          Row(
                                            children: [
                                              Container(
                                                padding: EdgeInsets.only(
                                                    right: MediaQuery.of(
                                                        context)
                                                        .size
                                                        .width /
                                                        24),
                                                child: Image.asset(
                                                  'assets/motostatus.png',
                                                  scale: _size.width>=600
                                                      ? 2.5
                                                      : 3.5,
                                                ),
                                              ),
                                              SizedBox(
                                                width: 5,
                                              ),
                                              Column(
                                                crossAxisAlignment:
                                                CrossAxisAlignment
                                                    .start,
                                                children: [
                                                  Container(
                                                      child: Text(
                                                        'Tank ${index}',
                                                        style: TextStyle(
                                                            color: Colors
                                                                .black,
                                                            fontSize: _size.width>=600
                                                                ? MediaQuery.of(context).size.height/50
                                                                : 13,
                                                            fontFamily:
                                                            'Proxima',
                                                            fontWeight:
                                                            FontWeight
                                                                .bold),
                                                      )),
                                                ],
                                              )
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                    Divider(
                                      color: Colors.grey[200],
                                      indent: 10,
                                      endIndent: 10,
                                    ),
                                    _loadingInProgress_status
                                        ? Center(
                                        child: Lottie.asset(
                                            'assets/status.json',
                                            height: 60,
                                            width: 60))
                                        : Container(
                                      padding: EdgeInsets.only(
                                          top: _size.width>=600  ? MediaQuery.of(context).size.height/90 : 0),
                                      child: FlutterSwitch(
                                          width:
                                          _size.width>=600  ? MediaQuery.of(context).size.height/10 : 70.0,
                                          height:
                                          _size.width>=600  ? MediaQuery.of(context).size.height/25 : 30.0,
                                          activeColor:
                                          Colors.green,
                                          inactiveColor:
                                          Colors.red,
                                          valueFontSize: 16.0,
                                          toggleSize: 20.0,
                                          value: motordatavaluelist[
                                          index]
                                              .motorStatus ==
                                              "On"
                                              ? on_off = true
                                              : on_off = false,
                                          borderRadius: 30.0,
                                          showOnOff: true,
                                          onToggle: (val) {
                                            setState(() {
                                              showDialog(
                                                  barrierDismissible: false,
                                                  context:
                                                  context,
                                                  builder:
                                                      (context) {
                                                    return CustomDialog(
                                                      content: '',
                                                      title: motorstatusvalue ==
                                                          "Off"
                                                          ? "Are You Sure To ON Motor !"
                                                          : "Are You Sure To OFF Motor !",
                                                      image: Icon(
                                                        Icons
                                                            .settings,
                                                        color: Colors
                                                            .white,
                                                        size: 40,
                                                      ),
                                                      colors: Colors
                                                          .green,
                                                      positiveBtnText:
                                                      "Yes",
                                                      negativeBtnText:
                                                      "No",
                                                      positiveBtnPressed:
                                                          () {
                                                        setState(
                                                                () {
                                                              getstatuschange(
                                                                  motordatavaluelist[index].id);
                                                            });
                                                      },
                                                      negativeBtnPressed:
                                                          () {
                                                        setState(
                                                                () {
                                                              getstatus(
                                                                  motordatavaluelist[index].id);
                                                            });
                                                      },
                                                    );
                                                  });
                                            });
                                          }),
                                    ),
                                  ]),
                                ),
                              ),
                            ))
                            : Container(),
                        flowmetercount != 0
                            ? Container(
                          padding: EdgeInsets.only(
                              left:
                              MediaQuery.of(context).size.width / 30),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Text(
                                'Flow Meter',
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: _size.width>=600  ? MediaQuery.of(context).size.height / 50 : 15,
                                    fontWeight: FontWeight.bold,
                                    fontFamily: 'Proxima'),
                              ),
                            ],
                          ),
                        )
                            : Container(),
                        flowmetercount != 0
                            ? GridView.builder(
                            shrinkWrap: true,
                            physics: NeverScrollableScrollPhysics(),
                            gridDelegate:
                            SliverGridDelegateWithFixedCrossAxisCount(
                                crossAxisCount: 2,
                                childAspectRatio:
                                _size.width>=600  ? MediaQuery.of(context).size.width /
                                    (MediaQuery.of(context).size.height / 2.3) : 5 / 4,
                                crossAxisSpacing: _size.width>=600  ? 60 : 10,
                                mainAxisSpacing: _size.width>=600  ? 20 : 10),
                            itemCount: flowdatavaluelist.length,
                            itemBuilder: (ctx, index) {
                              return GestureDetector(
                                onTap: () {
                                },
                                child: Container(
                                  margin: _size.width>=600? EdgeInsets.all(30):EdgeInsets.all(20),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(5)),
                                      shape: BoxShape.rectangle,
                                      color: Colors.white,
                                      boxShadow: [
                                        BoxShadow(
                                          color: Colors.grey
                                              .withOpacity(0.1),
                                          spreadRadius: 3,
                                          blurRadius: 3,
                                          offset: Offset(0,
                                              3), // changes position of shadow
                                        ),
                                      ]),
                                  child: Column(
                                    children: [
                                      Container(
                                          alignment: Alignment.center,
                                          width: _size.width>=600  ? 200 : 110,
                                          padding: EdgeInsets.only(
                                              top: _size.width>=600  ? MediaQuery.of(context).size.height / 50 : 18),
                                          child: Text(
                                            flowdatavaluelist[index]
                                                .name,
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontSize:
                                                _size.width>=600  ? MediaQuery.of(context).size.height / 50 : 15,
                                                fontFamily: 'Proxima',
                                                fontWeight:
                                                FontWeight.bold),
                                          )),
                                      SizedBox(
                                        height: MediaQuery.of(context)
                                            .size
                                            .height /
                                            60,
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                        MainAxisAlignment
                                            .spaceAround,
                                        children: [
                                          Row(
                                            children: [
                                              Container(
                                                padding: EdgeInsets.only(
                                                    right: MediaQuery.of(
                                                        context)
                                                        .size
                                                        .width /
                                                        24),
                                                child: Image.asset(
                                                  'assets/nitro.png',
                                                  scale: _size.width>=600  ? 1 : 2,
                                                ),
                                              ),
                                              Column(
                                                crossAxisAlignment:
                                                CrossAxisAlignment
                                                    .start,
                                                children: [
                                                  Container(
                                                      child: Text(
                                                        flowdatavaluelist[
                                                        index]
                                                            .label,
                                                        style: TextStyle(
                                                            color: Colors
                                                                .black54,
                                                            fontSize: _size.width>=600
                                                                ? MediaQuery.of(context).size.height / 50
                                                                : 13,
                                                            fontFamily:
                                                            'Proxima'),
                                                      )),
                                                  SizedBox(
                                                    height: 8,
                                                  ),
                                                  Container(
                                                      child: Text(
                                                          parsedJson !=
                                                              null
                                                              ? flowdatavaluelist[index].id.contains(parsedJson[
                                                          0])
                                                              ? mqttdatas
                                                              .mqttValue
                                                              : flowdatavaluelist[index]
                                                              .currrentFlow
                                                              : flowdatavaluelist[
                                                          index]
                                                              .currrentFlow,
                                                          style: TextStyle(
                                                              color: Color(
                                                                  0xff002251),
                                                              fontWeight:
                                                              FontWeight
                                                                  .bold,
                                                              fontSize:
                                                              _size.width>=600
                                                                  ? MediaQuery.of(context).size.height / 50
                                                                  : 15,
                                                              fontFamily:
                                                              'Proxima')))
                                                ],
                                              )
                                            ],
                                          )
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              );
                            })
                            : Container(),
                      ],
                    )),
              );
            }
            else{
              return NotificationListener<OverscrollIndicatorNotification>(
                child: SingleChildScrollView(
                    child: status
                        ? Container(
                      alignment: Alignment.center,
                      height: MediaQuery.of(context).size.height / 1.3,
                      width: MediaQuery.of(context).size.width,
                      child: Lottie.asset('assets/loading.json',
                          height: _size.width>=600  ? 80 : 60, width: _size.width>=600  ? 80 : 60),
                    )
                        : Column(
                      children: [
                        levelcount != 0
                            ? Container(
                          padding: EdgeInsets.only(
                              left:
                              MediaQuery.of(context).size.width / 30),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Text(
                                'Quantity Report',
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: _size.width>=600  ? 25 : 15,
                                    fontWeight: FontWeight.bold,
                                    fontFamily: 'Proxima'),
                              ),
                            ],
                          ),
                        )
                            : Container(),
                        levelcount != 0
                            ? levelcount != null
                            ? GridView.count(
                            shrinkWrap: true,
                            physics: NeverScrollableScrollPhysics(),
                            crossAxisCount: 2,
                            crossAxisSpacing: _size.width>=600  ? 200 : 10,
                            mainAxisSpacing: _size.width>=600  ? 20 : 10,
                            childAspectRatio:
                            (_size.width>=600  ? 5 / 3.5 : 4 / 3),
                            children: List.generate(
                              leveldatavaluelist.length,
                                  (index) => GestureDetector(
                                  onTap: () {
                                  },
                                  child: Container(
                                    margin: EdgeInsets.all(50),
                                    decoration: BoxDecoration(
                                        borderRadius:
                                        BorderRadius.all(
                                            Radius.circular(5)),
                                        shape: BoxShape.rectangle,
                                        color: Colors.white,
                                        boxShadow: [
                                          BoxShadow(
                                            color: Colors.grey
                                                .withOpacity(0.1),
                                            spreadRadius: 3,
                                            blurRadius: 3,
                                            offset: Offset(0,
                                                3), // changes position of shadow
                                          ),
                                        ]),
                                    child: Column(
                                        crossAxisAlignment:
                                        CrossAxisAlignment
                                            .center,
                                        children: [
                                          SizedBox(
                                            height: _size.width>=600  ? 20 : 8,
                                          ),
                                          Container(
                                              height: _size.width>=600 ?55:35,
                                              padding:
                                              EdgeInsets.only(
                                                  top: _size.width>=600
                                                      ? 15
                                                      : 3,
                                                  right: 10,
                                                  left: 10),
                                              child: Text(
                                                leveldatavaluelist[
                                                index]
                                                    .tankName,
                                                style: TextStyle(
                                                    color: Colors
                                                        .black,
                                                    fontSize: _size.width>=600
                                                        ? 22
                                                        : 13,
                                                    fontFamily:
                                                    'Proxima',
                                                    fontWeight:
                                                    FontWeight
                                                        .bold),
                                                textAlign: TextAlign
                                                    .center,
                                              )),
                                          Container(
                                            margin: EdgeInsets.only(
                                                top: _size.width>=600
                                                    ? 30
                                                    : 5),
                                            child: Row(
                                              mainAxisAlignment:
                                              MainAxisAlignment
                                                  .spaceAround,
                                              children: [
                                                Row(
                                                  children: [
                                                    Container(
                                                      padding: EdgeInsets.only(
                                                          right: MediaQuery.of(context)
                                                              .size
                                                              .width /
                                                              24),
                                                      child: Image
                                                          .asset(
                                                        'assets/tanks.png',
                                                        scale: _size.width>=600
                                                            ? 0.6
                                                            : 1,
                                                      ),
                                                    ),
                                                    Column(
                                                      crossAxisAlignment:
                                                      CrossAxisAlignment
                                                          .start,
                                                      children: [
                                                        Container(
                                                            child:
                                                            Text(
                                                              "${leveldatavaluelist[index].label} ${leveldatavaluelist[index].unit}",
                                                              style: TextStyle(
                                                                  color: Colors
                                                                      .black54,
                                                                  fontSize: _size.width>=600
                                                                      ? 22
                                                                      : 13,
                                                                  fontFamily:
                                                                  'Proxima'),
                                                            )),
                                                        SizedBox(
                                                          height: 8,
                                                        ),
                                                        Container(
                                                            child: Text(
                                                                parsedJson != null
                                                                    ? leveldatavaluelist[index].id.contains(parsedJson[0])
                                                                    ? mqttdatas.mqttValue
                                                                    : leveldatavaluelist[index].currentpercentage
                                                                    : leveldatavaluelist[index].currentpercentage,
                                                                style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: _size.width>=600  ? 22 : 13, fontFamily: 'Proxima'))),
                                                      ],
                                                    )
                                                  ],
                                                ),
                                              ],
                                            ),
                                          ),
                                        ]),
                                  )),
                            ))
                            : Container()
                            : Container(),
                        qacount != 0
                            ? Container(
                          padding: EdgeInsets.only(
                              left:
                              MediaQuery.of(context).size.width / 30),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Text(
                                'Quality Report',
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: _size.width>=600  ? 20 : 15,
                                    fontWeight: FontWeight.bold,
                                    fontFamily: 'Proxima'),
                              ),
                            ],
                          ),
                        )
                            : Container(),
                        qacount != 0
                            ? GridView.builder(
                            shrinkWrap: true,
                            physics: NeverScrollableScrollPhysics(),
                            gridDelegate:
                            SliverGridDelegateWithFixedCrossAxisCount(
                                crossAxisCount: 2,
                                childAspectRatio:
                                _size.width>=600  ? 5 / 3.5 : 4 / 3,
                                crossAxisSpacing: _size.width>=600  ? 200 : 10,
                                mainAxisSpacing: _size.width>=600  ? 20 : 10),
                            itemCount: qualitydatas.length,
                            itemBuilder: (ctx, index) {
                              return GestureDetector(
                                onTap: () {
                                },
                                child: Container(
                                  margin: EdgeInsets.all(45),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(5)),
                                      shape: BoxShape.rectangle,
                                      color: Colors.white,
                                      boxShadow: [
                                        BoxShadow(
                                          color: Colors.grey
                                              .withOpacity(0.1),
                                          spreadRadius: 3,
                                          blurRadius: 3,
                                          offset: Offset(0,
                                              3), // changes position of shadow
                                        ),
                                      ]),
                                  child: Column(
                                    children: [
                                      Container(
                                          padding:
                                          EdgeInsets.only(top: 20),
                                          child: Text(
                                            qualitydatas[index].name,
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontSize:
                                                _size.width>=600  ? 22 : 15,
                                                fontFamily: 'Proxima',
                                                fontWeight:
                                                FontWeight.bold),
                                          )),
                                      SizedBox(
                                        height: _size.width>=600  ? 25 : 20,
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                        MainAxisAlignment
                                            .spaceAround,
                                        children: [
                                          Row(
                                            children: [
                                              Container(
                                                padding: EdgeInsets.only(
                                                    right: MediaQuery.of(
                                                        context)
                                                        .size
                                                        .width /
                                                        24),
                                                child: Image.asset(
                                                  'assets/phlevel.png',
                                                  scale: _size.width>=600
                                                      ? 2
                                                      : 3.5,
                                                ),
                                              ),
                                              Column(
                                                crossAxisAlignment:
                                                CrossAxisAlignment
                                                    .start,
                                                children: [
                                                  Container(
                                                      child: Text(
                                                        'Current',
                                                        style: TextStyle(
                                                            color: Colors
                                                                .black54,
                                                            fontSize: _size.width>=600
                                                                ? 22
                                                                : 13,
                                                            fontFamily:
                                                            'Proxima'),
                                                      )),
                                                  SizedBox(
                                                    height: 8,
                                                  ),
                                                  Container(
                                                      child: Text(
                                                          parsedJson !=
                                                              null
                                                              ? qualitydatas[index].id.contains(parsedJson[
                                                          0])
                                                              ? mqttdatas
                                                              .mqttValue
                                                              : qualitydatas[index]
                                                              .currentvalue
                                                              : qualitydatas[
                                                          index]
                                                              .currentvalue,
                                                          style: TextStyle(
                                                              color: Color(
                                                                  0xff002251),
                                                              fontWeight:
                                                              FontWeight
                                                                  .bold,
                                                              fontSize:
                                                              _size.width>=600
                                                                  ? 22
                                                                  : 15,
                                                              fontFamily:
                                                              'Proxima')))
                                                ],
                                              )
                                            ],
                                          )
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              );
                            })
                            : Container(),
                        motordatacount != 0
                            ? Container(
                          padding: EdgeInsets.only(
                              left:
                              MediaQuery.of(context).size.width / 30),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Text(
                                'Motor Status',
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: _size.width>=600  ? 25 : 15,
                                    fontWeight: FontWeight.bold,
                                    fontFamily: 'Proxima'),
                              ),
                            ],
                          ),
                        )
                            : Container(),
                        motordatacount != 0
                            ? GridView.count(
                            shrinkWrap: true,
                            childAspectRatio:
                            (_size.width>=600  ? 5 / 3.8 : 4 / 3.7),
                            crossAxisSpacing: _size.width>=600  ? 180 : 10,
                            mainAxisSpacing: _size.width>=600  ? 40 : 10,
                            physics: NeverScrollableScrollPhysics(),
                            crossAxisCount: 2,
                            children: List.generate(
                              motordatacount,
                                  (index) => GestureDetector(
                                onTap: () {
                                },
                                child: Container(
                                  margin: EdgeInsets.all(45),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(5)),
                                      shape: BoxShape.rectangle,
                                      color: Colors.white,
                                      boxShadow: [
                                        BoxShadow(
                                          color: Colors.grey
                                              .withOpacity(0.1),
                                          spreadRadius: 3,
                                          blurRadius: 3,
                                          offset: Offset(0,
                                              3), // changes position of shadow
                                        ),
                                      ]),
                                  child: Column(children: [
                                    Container(
                                        padding:
                                        EdgeInsets.only(top: 25),
                                        child: Text(
                                          motordatavaluelist[index]
                                              .motorName,
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontSize:
                                              _size.width>=600  ? 22 : 15,
                                              fontFamily: 'Proxima',
                                              fontWeight:
                                              FontWeight.bold),
                                        )),
                                    Container(
                                      margin: EdgeInsets.only(
                                          top: _size.width>=600  ? 20 : 10),
                                      child: Row(
                                        mainAxisAlignment:
                                        MainAxisAlignment
                                            .spaceAround,
                                        children: [
                                          Row(
                                            children: [
                                              Container(
                                                padding: EdgeInsets.only(
                                                    right: MediaQuery.of(
                                                        context)
                                                        .size
                                                        .width /
                                                        24),
                                                child: Image.asset(
                                                  'assets/motostatus.png',
                                                  scale: _size.width>=600
                                                      ? 2
                                                      : 3.5,
                                                ),
                                              ),
                                              SizedBox(
                                                width: 5,
                                              ),
                                              Column(
                                                crossAxisAlignment:
                                                CrossAxisAlignment
                                                    .start,
                                                children: [
                                                  Container(
                                                      child: Text(
                                                        'Tank ${index}',
                                                        style: TextStyle(
                                                            color: Colors
                                                                .black,
                                                            fontSize: _size.width>=600
                                                                ? 22
                                                                : 13,
                                                            fontFamily:
                                                            'Proxima',
                                                            fontWeight:
                                                            FontWeight
                                                                .bold),
                                                      )),
                                                ],
                                              )
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                    Divider(
                                      color: Colors.grey[200],
                                      indent: 10,
                                      endIndent: 10,
                                    ),
                                    _loadingInProgress_status
                                        ? Center(
                                        child: Lottie.asset(
                                            'assets/status.json',
                                            height: 60,
                                            width: 60))
                                        : Container(
                                      padding: EdgeInsets.only(
                                          top: _size.width>=600  ? 10 : 0),
                                      child: FlutterSwitch(
                                          width:
                                          _size.width>=600  ? 120 : 70.0,
                                          height:
                                          _size.width>=600  ? 50 : 30.0,
                                          activeColor:
                                          Colors.green,
                                          inactiveColor:
                                          Colors.red,
                                          valueFontSize: 16.0,
                                          toggleSize: 20.0,
                                          value: motordatavaluelist[
                                          index]
                                              .motorStatus ==
                                              "On"
                                              ? on_off = true
                                              : on_off = false,
                                          borderRadius: 30.0,
                                          showOnOff: true,
                                          onToggle: (val) {
                                            setState(() {
                                              showDialog(
                                                  context:
                                                  context,
                                                  builder:
                                                      (context) {
                                                    return CustomDialog(
                                                      content: '',
                                                      title: motorstatusvalue ==
                                                          "Off"
                                                          ? "Are You Sure To ON Motor !"
                                                          : "Are You Sure To OFF Motor !",
                                                      image: Icon(
                                                        Icons
                                                            .settings,
                                                        color: Colors
                                                            .white,
                                                        size: 40,
                                                      ),
                                                      colors: Colors
                                                          .green,
                                                      positiveBtnText:
                                                      "Yes",
                                                      negativeBtnText:
                                                      "No",
                                                      positiveBtnPressed:
                                                          () {
                                                        setState(
                                                                () {
                                                              getstatuschange(
                                                                  motordatavaluelist[index].id);
                                                            });
                                                      },
                                                      negativeBtnPressed:
                                                          () {
                                                        setState(
                                                                () {
                                                              getstatus(
                                                                  motordatavaluelist[index].id);
                                                            });
                                                      },
                                                    );
                                                  });
                                            });
                                          }),
                                    ),
                                  ]),
                                ),
                              ),
                            ))
                            : Container(),
                        flowmetercount != 0
                            ? Container(
                          padding: EdgeInsets.only(
                              left:
                              MediaQuery.of(context).size.width / 30),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Text(
                                'Flow Meter',
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: _size.width>=600  ? 25 : 15,
                                    fontWeight: FontWeight.bold,
                                    fontFamily: 'Proxima'),
                              ),
                            ],
                          ),
                        )
                            : Container(),
                        flowmetercount != 0
                            ? GridView.builder(
                            shrinkWrap: true,
                            physics: NeverScrollableScrollPhysics(),
                            gridDelegate:
                            SliverGridDelegateWithFixedCrossAxisCount(
                                crossAxisCount: 2,
                                childAspectRatio:
                                _size.width>=600  ? 5 / 3.5 : 4 / 2.8,
                                crossAxisSpacing: _size.width>=600  ? 200 : 10,
                                mainAxisSpacing: _size.width>=600  ? 20 : 10),
                            itemCount: flowdatavaluelist.length,
                            itemBuilder: (ctx, index) {
                              return GestureDetector(
                                onTap: () {
                                },
                                child: Container(
                                  margin: EdgeInsets.all(50),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(5)),
                                      shape: BoxShape.rectangle,
                                      color: Colors.white,
                                      boxShadow: [
                                        BoxShadow(
                                          color: Colors.grey
                                              .withOpacity(0.1),
                                          spreadRadius: 3,
                                          blurRadius: 3,
                                          offset: Offset(0,
                                              3), // changes position of shadow
                                        ),
                                      ]),
                                  child: Column(
                                    children: [
                                      Container(
                                          alignment: Alignment.center,
                                          width: _size.width>=600  ? 200 : 110,
                                          padding: EdgeInsets.only(
                                              top: _size.width>=600  ? 25 : 18),
                                          child: Text(
                                            flowdatavaluelist[index]
                                                .name,
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontSize:
                                                _size.width>=600  ? 22 : 15,
                                                fontFamily: 'Proxima',
                                                fontWeight:
                                                FontWeight.bold),
                                          )),
                                      SizedBox(
                                        height: MediaQuery.of(context)
                                            .size
                                            .height /
                                            20,
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                        MainAxisAlignment
                                            .spaceAround,
                                        children: [
                                          Row(
                                            children: [
                                              Container(
                                                padding: EdgeInsets.only(
                                                    right: MediaQuery.of(
                                                        context)
                                                        .size
                                                        .width /
                                                        24),
                                                child: Image.asset(
                                                  'assets/nitro.png',
                                                  scale: _size.width>=600  ? 0.8 : 2,
                                                ),
                                              ),
                                              Column(
                                                crossAxisAlignment:
                                                CrossAxisAlignment
                                                    .start,
                                                children: [
                                                  Container(
                                                      child: Text(
                                                        flowdatavaluelist[
                                                        index]
                                                            .label,
                                                        style: TextStyle(
                                                            color: Colors
                                                                .black54,
                                                            fontSize: _size.width>=600
                                                                ? 22
                                                                : 13,
                                                            fontFamily:
                                                            'Proxima'),
                                                      )),
                                                  SizedBox(
                                                    height: 8,
                                                  ),
                                                  Container(
                                                      child: Text(
                                                          parsedJson !=
                                                              null
                                                              ? flowdatavaluelist[index].id.contains(parsedJson[
                                                          0])
                                                              ? mqttdatas
                                                              .mqttValue
                                                              : flowdatavaluelist[index]
                                                              .currrentFlow
                                                              : flowdatavaluelist[
                                                          index]
                                                              .currrentFlow,
                                                          style: TextStyle(
                                                              color: Color(
                                                                  0xff002251),
                                                              fontWeight:
                                                              FontWeight
                                                                  .bold,
                                                              fontSize:
                                                              _size.width>=600
                                                                  ? 22
                                                                  : 15,
                                                              fontFamily:
                                                              'Proxima')))
                                                ],
                                              )
                                            ],
                                          )
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              );
                            })
                            : Container(),
                      ],
                    )),
              );
            }
          },
        ),
      ),
    );
  }
}

class CustomDialog extends StatelessWidget {
  final String title, content, positiveBtnText, negativeBtnText;
  final GestureTapCallback positiveBtnPressed, negativeBtnPressed;
  final Icon image;
  final Color colors;

  CustomDialog(
      {@required this.title,
        @required this.content,
        @required this.image,
        @required this.positiveBtnText,
        @required this.negativeBtnText,
        @required this.positiveBtnPressed,
        @required this.negativeBtnPressed,
        @required this.colors});

  @override
  Widget build(BuildContext context) {
    return Dialog(
      elevation: 0,
      backgroundColor: Colors.transparent,
      child: _buildDialogContent(context),
    );
  }

  Widget _buildDialogContent(BuildContext context) {
    return Stack(
      alignment: Alignment.topCenter,
      children: <Widget>[
        Container(
          // Bottom rectangular box
          margin: EdgeInsets.only(top: 40),
          // to push the box half way below circle
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(12),
          ),
          padding: EdgeInsets.only(top: 35, left: 20, right: 20),
          // spacing inside the box
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text(
                title,
                style: TextStyle(
                    color: Colors.black54,
                    fontSize: 16.0,
                    fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: 16,
              ),
              ButtonBar(
                buttonMinWidth: 100,
                alignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  FlatButton(
                    child: Text(
                      negativeBtnText,
                      style: TextStyle(
                          color: Colors.red,
                          fontSize: 16.0,
                          fontWeight: FontWeight.bold),
                    ),
                    onPressed: negativeBtnPressed,
                  ),
                  FlatButton(
                    child: Text(
                      positiveBtnText,
                      style: TextStyle(
                          color: Colors.green,
                          fontSize: 16.0,
                          fontWeight: FontWeight.bold),
                    ),
                    onPressed: positiveBtnPressed,
                  ),
                ],
              ),
            ],
          ),
        ),
        CircleAvatar(
          backgroundColor: colors, // Top Circle with icon
          maxRadius: 35.0,
          child: image,
        ),
      ],
    );
  }
}
