// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'data_model.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class DataModelAdapter extends TypeAdapter<DataModel> {
  @override
  final int typeId = 0;

  @override
  DataModel read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return DataModel(
      leveldata: fields[2] as dynamic,
      qualitydata: fields[4] as dynamic,
      motordata: fields[3] as dynamic,
      flowmeterdata: fields[5] as dynamic,
      mqttId: fields[0] as dynamic,
      mqttValue: fields[1] as dynamic,
    );
  }

  @override
  void write(BinaryWriter writer, DataModel obj) {
    writer
      ..writeByte(6)
      ..writeByte(0)
      ..write(obj.mqttId)
      ..writeByte(1)
      ..write(obj.mqttValue)
      ..writeByte(2)
      ..write(obj.leveldata)
      ..writeByte(3)
      ..write(obj.motordata)
      ..writeByte(4)
      ..write(obj.qualitydata)
      ..writeByte(5)
      ..write(obj.flowmeterdata);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is DataModelAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
