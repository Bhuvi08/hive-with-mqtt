import 'package:hive/hive.dart';
part 'data_model.g.dart';

@HiveType(typeId: 0)
class DataModel {
  @HiveField(0)
  var mqttId;
  @HiveField(1)
  var mqttValue;
  @HiveField(2)
  var leveldata;
  @HiveField(3)
  var motordata;
  @HiveField(4)
  var qualitydata;
  @HiveField(5)
  var flowmeterdata;

  DataModel({this.leveldata,this.qualitydata,this.motordata,this.flowmeterdata,this.mqttId,this.mqttValue,});
}